import React, {useState} from 'react';
import GameSquare from "../components/GameSquare";
import GameCounter from "../components/GameCounter";
import GameButton from "../components/GameButton";

const GameField = () => {
    const [counter, setCounter] = useState(0);
    const [found, setFound] = useState(false);

    const getRandom = (min, max) => {
        const random = min + Math.random() * (max - min);
        return Math.round(random);
    };

    const createInitialData = () => {
        const initialSquares = [];
        for (let i = 1; i < 37; i++) {
            const newObject = {
                id: i,
                hasItem: false,
                className: ['square'],
            };
            initialSquares.push(newObject);
        }
        initialSquares[getRandom(0, initialSquares.length - 1)].hasItem = true;
        return initialSquares;
    };

    const [squares, setSquares] = useState(createInitialData());

    const clickSquare = (id) => {
        if (found === false) {
            setSquares(squares.map(item => {
                if (id === item.id) {
                    if (item.hasItem === false) {
                        return {
                            ...item,
                            className: ['square', 'wrong']
                        };
                    } else if (item.hasItem === true) {
                        setFound(true);
                        return {
                            ...item,
                            className: ['square', 'right']
                        };
                    }
                }
                return item;
            }));
            setCounter(counter + 1);
        } else {
            alert("Reset game!");
        }
    };

    const clickBtn = () => {
        setSquares(createInitialData());
        setCounter(0);
        setFound(false);
    };

    return (
        <div className="container">
            {found === true
                ? <h1>You won!</h1>
                : null
            }
            <div className="game-field">
                {squares.map(item => {
                    return <GameSquare
                        classname={item.className}
                        onSquareClick={() => clickSquare(item.id)}
                        key={item.id}
                    />
                })}
            </div>
            <GameCounter counter={counter}/>
            <GameButton onButtonClick={clickBtn}/>
        </div>
    );
};

export default GameField;