import React from 'react';

const GameButton = ({ onButtonClick }) => {
    return (
        <button className="reset-btn" onClick={onButtonClick}>
            Reset
        </button>
    );
};

export default GameButton;