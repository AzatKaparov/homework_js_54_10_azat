import React from 'react';

const GameCounter = ({ counter }) => {
    return (
        <div>
            <h3 className="counter">Tries: {counter}</h3>
        </div>
    );
};

export default GameCounter;