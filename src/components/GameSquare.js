import React from 'react';

const GameSquare = ({ classname, onSquareClick }) => {
    return (
        <div className={classname.join(" ")} onClick={onSquareClick}/>
    );
};

export default GameSquare;