import GameField from "./containers/GameField";
import './containers/Game.css';

function App() {
  return (
      <GameField/>
  );
}

export default App;
